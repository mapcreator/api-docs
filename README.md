# Mapcreator API Documentation

This uses a node script `scripts/generateTables.js` to download the api docs then generates a static
website using [Middleman](https://middlemanapp.com/). These are then uploaded to AWS S3 and
cloudfront is purge. Note that the browser cache won't be so to see changes open a private window.
