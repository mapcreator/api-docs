#!/bin/bash
set -eo pipefail
scripts/prepare.sh
apt-get -y install build-essential ruby ruby-dev nodejs npm zlib1g-dev
npm run generate-model-tables
gem install bundler:1.17.3
bundle install
bundle exec middleman build --clean --verbose
# fix up stuff that broke during the build
mv build/stylesheets/print build/stylesheets/print.css
mv build/stylesheets/screen build/stylesheets/screen.css
