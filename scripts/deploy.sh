#!/bin/bash
set -eo pipefail
scripts/prepare.sh
apt-get -y install awscli
aws s3 sync build/ "s3://$S3_BUCKET"
aws cloudfront create-invalidation --distribution-id "$CLOUDFRONT_ID" --paths="/*"
