#!/bin/bash
set -e
# the first 3 lines of this get the default gateway of the container
# which is the host (aka gitlab runner) we have to do it this way because
# iproute2 and other tools aren't installed
# modified from https://stackoverflow.com/a/14725655
hex=$(awk '$2 == "00000000" && $8 == "00000000" { print $3 }' < /proc/net/route)
printf -v int "%d\n" 0x${hex:6:2}${hex:4:2}${hex:2:2}${hex:0:2}
printf -v gateway "%s.%s.%s.%s" $(($int>>24)) $(($int>>16&255)) $(($int>>8&255)) $(($int&255))
echo 'Acquire::http { Proxy "http://'"$gateway"':3142"; };' >> /etc/apt/apt.conf.d/01proxy
apt-get update
